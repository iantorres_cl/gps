<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ian Torres</title>

    <!-- Bootstrap -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="app">
    <records-viewer :records="{{ $records }}" inline-template>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        </div>
                        <table class="table table-sm table-responsive">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Patente</th>
                                    <th>Fecha</th>
                                    <th>Información</th>
                                    <th>Distancia Recorrida</th>
                                    <th>Distancia Acumulada</th>
                                    <th>Velocidad</th>
                                    <th>Coordenadas</th>
                                    <th>Coordenadas Inicial</th>
                                    <th>Coordenadas Final</th>
                                    <th>Fecha Inicial</th>
                                    <th>Fecha Final</th>
                                    <th>Duración</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(record, index) in records" is="record" :records="records" :record="record" :index="index"></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </records-viewer>
</div>

<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>