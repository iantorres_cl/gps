var moment = require('moment');

moment.locale('es');

Vue.component('record', {
    props: ['records', 'record', 'index'],
    template: `
        <tr v-if="isRenderable">
            <td>{{ index }}</td>
            <td>{{ record.placa }}</td>
            <td>{{ record.fecha }}</td>
            <td>{{ status }}</td>
            <td>{{ differencesBetweenPrev }}</td>
            <td>{{ distancia }}</td>
            <td>{{ record.velocidad }} Kms/h</td>
            <td>{{ coordenadas }}</td>
            <td>{{ coordenadasInicial }}</td>
            <td>{{ coordenadasFinal }}</td>
            <td>{{ fechaInicial }}</td>
            <td>{{ fechaFinal }}</td>
            <td>{{ duracion }}</td>
        </tr>`,
    data: function () {
        return {
        }
    },
    computed: {
        /**
         * Obtiene el estado del registro
         * @returns {string}
         */
        status: function () {
            let self = this;
            if ( self.record.tipo === "ST300STT" ) {
                if ( self.record.estado_io.charAt(0) === "1" ) {
                    if( self.prev.estado_io.charAt(0) === "1" ) {
                        return "En marcha";
                    } else {
                        return "Motor Encendido";
                    }
                }
                return "Motor Apagado";
            } else {
                if ( self.record.modo === "33" ) {
                  return "Encendido de motor"
                }
                return "Apagado de motor";
            }
        },
        /**
         * Obtiene la validez del registro
         * @returns {string}
         */
        fix: function () {
            let self = this;

            if ( self.record.fix === "1" ) {
                return "Evento válido";
            }
            return "No válido"
        },
        /**
         * Obtiene las coordenadas del registro concatenadas
         * @returns {string}
         */
        coordenadas: function () {
            return this.record.latitud + "," + this.record.longitud;
        },
        /**
         * Obtiene la distancia mutada a kilometros
         * @returns {string}
         */
        distancia: function () {
            let raw = this.record.distancia / 1000;
            let kilometros = parseInt(raw);

            return kilometros + " Kms";
        },
        /**
         * Obtiene el registro previo al actual
         * @returns {*}
         */
        prev: function () {
            return this.records[this.index - 1];
        },
        /**
         * Obtiene el registro siguiente al actual
         * @returns {*}
         */
        next: function () {
            return this.records[this.index + 1];
        },
        /**
         * Conclusión si se imprime o no la fila
         * @returns {boolean}
         */
        isRenderable: function () {
            let self = this;
            if ( self.eventIsValid &&
                 self.status === "Motor Apagado" &&
                 self.prevDistanceIsSame &&
                 self.nextDistanceIsSame ) {
                return false;
            } else {

                if ( self.eventIsValid &&
                     self.status === "Motor Encendido" &&
                     self.nextDistanceIsSame &&
                     self.prevDistanceIsSame ) {
                    return false;
                } else {
                    if( self.status === "Motor Encendido" &&
                        self.speedIsMoreThanZero === false ) {
                        return false;
                    } else {
                        if ( self.status == "En marcha" && self.differencesBetweenPrev === "0 Kms") {
                            return false;
                        } else {
                            return true;
                        }
                    }
                }
            }
        },
        /**
         * Consulta si tiene la misma distancia con el registro anterior
         * @returns {boolean}
         */
        prevDistanceIsSame: function () {
            let self = this;
            if (self.prev === undefined) {
                return false;
            } else {
                if (self.prev.distancia == self.record.distancia) {
                    return true;
                } else {
                    return false;
                }
            }
        },
        /**
         * Consulta si tiene la misma distancia con el siguiente registro
         * @returns {boolean}
         */
        nextDistanceIsSame: function () {
            let self = this;
            if (self.next === undefined) {
                return false;
            } else {
                if (self.next.distancia == self.record.distancia) {
                    return true;
                } else {
                    return false;
                }
            }
        },
        /**
         * Consulta si el evento es valido
         * @returns {boolean}
         */
        eventIsValid: function () {
            if (this.fix === "Evento válido") {
                return true;
            } else {
                return false;
            }
        },
        /**
         * Consulta si la velocidad es mayor a 0 (ralentí)
         * @returns {boolean}
         */
        speedIsMoreThanZero: function () {
            return parseInt(this.record.velocidad) > 0;
        },
        /**
         * Obtiene la diferencia entre el registro actual y el previo
         * @returns {string}
         */
        differencesBetweenPrev: function() {

            if(this.prev !== undefined && this.status === "En marcha") {
                let kilometros = (this.record.distancia - this.prev.distancia) / 1000;

                return kilometros + " Kms";
            } else {
                return "";
            }
        },
        /**
         * Obtiene las coordenadas iniciales
         * @returns {string}
         */
        coordenadasInicial: function () {
            let self = this;
            if ( self.status === "Encendido de motor" && self.next.estado_io.charAt(0) === "1"){
                return this.record.latitud + "," + this.record.longitud;
            } else {
                return "";
            }
        },
        /**
         * Obtiene las coordenadas finales
         * @returns {string}
         */
        coordenadasFinal: function () {
            let self = this;

            if ( self.status === "Encendido de motor") {

                let founded = false;
                let count = 0;

                while (!founded) {
                    let current = self.records[self.index + count];

                    if(current.modo === "34") {
                        founded = true;

                        return current.latitud + "," + current.longitud;
                    }

                    count = count + 1;
                }
            } else {
                return "";
            }
        },
        /**
         * Obtiene la fecha inicial parseada
         * @returns {string}
         */
        fechaInicial: function () {
            let self = this;
            if ( self.status === "Encendido de motor" && self.next.estado_io.charAt(0) === "1"){
                return moment(this.record.fecha).format("dddd, MMMM Do YYYY, h:mm:ss a");
            } else {
                return "";
            }
        },
        /**
         * Obtiene la fecha final parseada
         * @returns {string}
         */
        fechaFinal: function () {
            let self = this;

            if ( self.status === "Encendido de motor") {

                let founded = false;
                let count = 0;

                while (!founded) {
                    let current = self.records[self.index + count];

                    if(current.modo === "34") {
                        founded = true;

                        return moment(current.fecha).format("dddd, MMMM Do YYYY, h:mm:ss a");
                    }

                    count = count + 1;
                }
            } else {
                return "";
            }
        },
        /**
         * Obtiene la duración del viaje humanizada
         * @returns {string}
         */
        duracion: function () {
            let self = this;

            if ( self.status === "Encendido de motor") {

                let notFounded = true;
                let count = 0;

                while (notFounded) {
                    let current = self.records[self.index + count];

                    if(current.modo === "34") {
                        notFounded = false;

                        let start = moment(self.record.fecha);
                        let end = moment(current.fecha);
                        return moment.duration(start.diff(end)).humanize();
                    }

                    count = count + 1;
                }
            } else {
                return "";
            }
        }
    }
});
